package net.mineclick.core.messenger;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

@Getter
@Setter
public class Message {
    private static long count = 0;
    @Setter(AccessLevel.NONE)
    private transient final CountDownLatch latch = new CountDownLatch(1);
    @Setter(AccessLevel.NONE)
    private transient Message responseMessage;
    private transient Consumer<Message> responseConsumer;

    private String uniqueId;
    private Action action;
    private Response response;

    public Message() {
        uniqueId = Messenger.getI().getServerId() + count++;
    }

    public void send() {
        Messenger.getI().send(this);
    }

    public void send(Response response) {
        this.response = response;
        send();
    }

    public boolean isOk() {
        return response != null && response.equals(Response.OK);
    }

    public void send(Action action) {
        this.action = action;
        send();
    }

    public void wrap(Function<Action, Response> wrapper) {
        wrap(action, wrapper);
    }

    public <T> void wrap(T action, Function<T, Response> wrapper) {
        try {
            Response response = wrapper.apply(action);
            if (response == null)
                return;

            send(response);
        } catch (Exception e) {
            e.printStackTrace();
            send(Response.ERROR);
        }
    }

    @SneakyThrows
    public Message blockResponse() {
        latch.await(getTimeout() + 3, TimeUnit.SECONDS);
        return responseMessage;
    }

    public void handleResponse(Message message) {
        if (responseConsumer != null) {
            responseConsumer.accept(message);
        }
        onResponse(message);
        unblock(message);
    }

    public void handleTimeout() {
        if (responseConsumer != null) {
            responseConsumer.accept(null);
        }
        onTimeout();
        unblock(null);
    }

    public void onReceive() {
    }

    public void onResponse(Message responseMessage) {
    }

    public void onTimeout() {
    }

    public int getTimeout() {
        return 5;
    }

    private void unblock(Message responseMessage) {
        this.responseMessage = responseMessage;
        latch.countDown();
    }
}
