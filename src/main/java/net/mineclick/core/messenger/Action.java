package net.mineclick.core.messenger;

public enum Action {
    GET,
    POST,
    UPDATE,
    DELETE
}
