package net.mineclick.core.messenger;

import com.google.gson.JsonElement;
import lombok.Getter;

@Getter
public class MessageWrapper {
    private String sender;
    private String messageName;
    private JsonElement message;

    public MessageWrapper(Message message, Messenger messenger) {
        sender = messenger.getServerId();
        messageName = messenger.getMessageName(message.getClass());
        this.message = messenger.getGson().toJsonTree(message);
    }
}
