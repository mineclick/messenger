package net.mineclick.core.messenger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.redisWrapper.RedisWrapper;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

@Getter
public class Messenger {
    @Getter
    private static Messenger i;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);
    private final String uuid = UUID.randomUUID().toString(); // A unique uuid for this messenger instance, different from serverId
    private final String serverId;
    private final Gson gson;
    private final Map<String, Class<Message>> messageNameMap = new HashMap<>(); // message name:message class
    private final Map<String, Message> awaitingResponse = new HashMap<>(); // message id:message
    private final RedisWrapper redisWrapper;
    private Logger logger = Logger.getLogger(Messenger.class.getCanonicalName());
    @Setter
    private boolean debug = Boolean.parseBoolean(System.getenv("MC_DEBUG"));
    private boolean currentNode;

    public Messenger(String serverId, RedisWrapper redisWrapper) {
        this(serverId, redisWrapper, null, null);
    }

    public Messenger(String serverId, RedisWrapper redisWrapper, Logger logr) {
        this(serverId, redisWrapper, logr, null);
    }

    public Messenger(String serverId, RedisWrapper redisWrapper, Logger logr, Gson gson) {
        i = this;
        if (logr != null) {
            logger = logr;
        }

        if (debug) {
            logger.info("Loading messenger...");
        }

        this.serverId = serverId;
        this.redisWrapper = redisWrapper;

        if (gson == null) {
            this.gson = new GsonBuilder()
                    .registerTypeAdapter(InstantConverter.TYPE, new InstantConverter())
                    .create();
        } else {
            this.gson = gson;
        }

        try (ScanResult scan = new ClassGraph().enableAllInfo().acceptPackages("net.mineclick").scan()) {
            for (Class<?> aClass : scan.getSubclasses(Message.class.getName()).loadClasses()) {
                messageNameMap.put(getMessageName(aClass), (Class<Message>) aClass);
            }
        }

        redisWrapper.subscribe("messenger");
        redisWrapper.acceptMessageConsumer(onMessage());

        // In case of a swarm node, check for the currently accepting node with the same serverId
        scheduler.scheduleAtFixedRate(() -> {
            String key = "messenger.currentNode:" + serverId;
            redisWrapper.get(key, current -> {
                if (current == null) {
                    redisWrapper.set(key, uuid); // Don't yet assume that this node will be the current
                    redisWrapper.expire(key, 2);
                } else if (current.equals(uuid)) { // If the set call above won the race, this node is the current
                    if (!currentNode) {
                        logger.info("Messenger node set to current");
                    }

                    currentNode = true;
                    redisWrapper.expire(key, 2);
                    return;
                }

                if (currentNode) {
                    logger.info("Messenger node removed from current");
                }
                currentNode = false;
            });
        }, 2, 1, TimeUnit.SECONDS);
    }

    public void addClassLoaded(ClassLoader classLoader) {
        try (ScanResult scan = new ClassGraph().enableAllInfo().addClassLoader(classLoader).scan()) {
            for (Class<?> aClass : scan.getSubclasses(Message.class.getName()).loadClasses()) {
                messageNameMap.put(getMessageName(aClass), (Class<Message>) aClass);
            }
        }
    }

    public String getMessageName(Class<?> clazz) {
        String name;
        MessageName annotation = clazz.getAnnotation(MessageName.class);
        if (annotation != null) {
            name = annotation.value();
        } else {
            logger.warning("MessageName is missing (and is recommended) in class " + clazz.getSimpleName());

            // Apply camelCase
            char[] c = clazz.getSimpleName()
                    .replace("Parser", "") //Legacy support
                    .toCharArray();
            c[0] = Character.toLowerCase(c[0]);
            name = new String(c);
        }
        return name;
    }

    private Consumer<String> onMessage() {
        return msg -> {
            if (msg.isEmpty())
                return;

            try {
                MessageWrapper wrapper = gson.fromJson(msg, MessageWrapper.class);
                // ignore messages sent from the same node
                if (wrapper.getSender().equals(serverId)) {
                    return;
                }

                Class<Message> messageClass = messageNameMap.get(wrapper.getMessageName());
                if (messageClass == null) {
                    return;
                }
                Message message = gson.fromJson(wrapper.getMessage(), messageClass);

                Message waitingResponse = awaitingResponse.remove(message.getUniqueId());
                if (waitingResponse != null) {
                    if (debug) {
                        logger.info("Received response: " + wrapper.getMessageName());
                        logger.info(wrapper.getMessage().toString());
                    }

                    Message responseMessage = gson.fromJson(wrapper.getMessage(), waitingResponse.getClass());
                    waitingResponse.handleResponse(responseMessage);
                    return;
                }

                // ignore response messages that were not sent from this node
                if (message.getResponse() != null) {
                    return;
                }

                if (currentNode) {
                    if (debug) {
                        logger.info("Received message: " + wrapper.getMessageName());
                        logger.info(wrapper.getMessage().toString());
                    }

                    message.onReceive();
                } else {
                    if (debug) {
                        logger.info("Ignored message: " + wrapper.getMessageName());
                    }
                }
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Messenger exception", e);
            }
        };
    }

    public boolean send(Message message) {
        if (awaitingResponse.containsKey(message.getUniqueId())) {
            return false;
        }

        awaitingResponse.put(message.getUniqueId(), message);
        scheduler.schedule(() -> {
            if (awaitingResponse.containsKey(message.getUniqueId())) {
                awaitingResponse.remove(message.getUniqueId()).handleTimeout();
            }
        }, message.getTimeout(), TimeUnit.SECONDS);

        String json = gson.toJson(new MessageWrapper(message, this));
        redisWrapper.send(json);

        if (debug) {
            logger.info("Sent message: " + getMessageName(message.getClass()));
            logger.info(json);
        }

        return true;
    }
}