package net.mineclick.core.messenger;

public enum Response {
    OK, NOT_FOUND, ERROR
}
