package net.mineclick.core.messenger.redisWrapper;

import net.mineclick.core.messenger.Messenger;
import org.springframework.data.redis.connection.ReactiveSubscription;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.ReactiveRedisMessageListenerContainer;

import java.time.Duration;
import java.util.function.Consumer;
import java.util.logging.Level;

public class LettuceWrapper implements RedisWrapper {
    private final ReactiveRedisTemplate<String, String> redisTemplate;
    private final ReactiveRedisMessageListenerContainer container;
    private String channel;
    private Consumer<String> messageConsumer;

    public LettuceWrapper(ReactiveRedisTemplate<String, String> redisTemplate, ReactiveRedisMessageListenerContainer container) {
        this.redisTemplate = redisTemplate;
        this.container = container;
    }

    @Override
    public void subscribe(String channel) {
        this.channel = channel;
        container.receive(new ChannelTopic(channel))
                .map(ReactiveSubscription.Message::getMessage)
                .onErrorContinue((throwable, value) -> Messenger.getI().getLogger().log(Level.SEVERE, "Subscriber exception", throwable))
                .subscribe(msg -> messageConsumer.accept(msg));
    }

    @Override
    public void acceptMessageConsumer(Consumer<String> messageConsumer) {
        this.messageConsumer = messageConsumer;
    }

    @Override
    public void send(String message) {
        if (channel == null) {
            Messenger.getI().getLogger().severe("Messenger redis (Lettuce) channel not defined");
            return;
        }

        redisTemplate.convertAndSend(channel, message).subscribe();
    }

    @Override
    public void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value).subscribe();
    }

    @Override
    public void get(String key, Consumer<String> consumer) {
        redisTemplate.opsForValue().get(key)
                .doOnSuccess(consumer)
                .subscribe();
    }

    @Override
    public void expire(String key, int seconds) {
        redisTemplate.expire(key, Duration.ofSeconds(seconds)).subscribe();
    }
}
