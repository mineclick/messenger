package net.mineclick.core.messenger.redisWrapper;

import net.mineclick.core.messenger.Messenger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;

public class JedisWrapper extends JedisPubSub implements RedisWrapper {
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);
    private final JedisPool jedisPool;
    private Consumer<String> messageConsumer;

    private String redisUrl;
    private int redisPort;
    private String redisPassword;

    public JedisWrapper(String redisUrl, int redisPort, String redisPassword) {
        this.redisUrl = redisUrl;
        this.redisPort = redisPort;
        this.redisPassword = redisPassword;
        if (redisPassword != null && redisPassword.isEmpty()) {
            redisPassword = null;
        }

        JedisPoolConfig config = new JedisPoolConfig();
        jedisPool = new JedisPool(config, redisUrl, redisPort, 2000, redisPassword);

        try (Jedis jedis = jedisPool.getResource()) {
            if (!"PONG".equals(jedis.ping())) {
                System.out.println("JEDIS RECEIVED NO PING RESPONSE! Check your config");
            }
        } catch (Exception e) {
            System.out.println("exception was thrown when pinging Jedis");
            System.out.println(e.getMessage());
        }
    }

    public JedisWrapper(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    @Override
    public void subscribe(String channel) {
        scheduler.scheduleAtFixedRate(() -> {
            try (Jedis jedis = new Jedis(redisUrl, redisPort)) {
                if (redisPassword != null) {
                    jedis.auth(redisPassword);
                }

                jedis.subscribe(this, channel);
            } catch (Exception e) {
                Messenger.getI().getLogger().log(Level.SEVERE, "exception was thrown in the Jedis subscription handler", e);
            }

            Messenger.getI().getLogger().severe("Messenger Jedis died! Trying again in 1 second...");
        }, 0, 1, TimeUnit.SECONDS);
    }

    @Override
    public void acceptMessageConsumer(Consumer<String> messageConsumer) {
        this.messageConsumer = messageConsumer;
    }

    @Override
    public void send(String message) {
        scheduler.submit(() -> {
            try (Jedis jedis = jedisPool.getResource()) {
                jedis.publish("messenger", message);
            } catch (Exception e) {
                Messenger.getI().getLogger().log(Level.SEVERE, "exception was thrown when sending a message", e);
            }
        });
    }

    @Override
    public void set(String key, String value) {
        scheduler.submit(() -> {
            try (Jedis jedis = jedisPool.getResource()) {
                jedis.set(key, value);
            } catch (Exception e) {
                Messenger.getI().getLogger().log(Level.SEVERE, "exception was thrown when settings a Jedis value", e);
            }
        });
    }

    @Override
    public void get(String key, Consumer<String> consumer) {
        scheduler.submit(() -> {
            try (Jedis jedis = jedisPool.getResource()) {
                consumer.accept(jedis.get(key));
            } catch (Exception e) {
                Messenger.getI().getLogger().log(Level.SEVERE, "exception was thrown when getting a Jedis value", e);
            }
        });
    }

    @Override
    public void expire(String key, int seconds) {
        scheduler.submit(() -> {
            try (Jedis jedis = jedisPool.getResource()) {
                jedis.expire(key, seconds);
            } catch (Exception e) {
                Messenger.getI().getLogger().log(Level.SEVERE, "exception was thrown when expiring a Jedis key", e);
            }
        });
    }

    @Override
    public void onMessage(String channel, String msg) {
        messageConsumer.accept(msg);
    }
}
