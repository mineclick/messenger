package net.mineclick.core.messenger.redisWrapper;

import java.util.function.Consumer;

public interface RedisWrapper {
    void subscribe(String channel);

    void acceptMessageConsumer(Consumer<String> messageConsumer);

    void send(String message);

    void set(String key, String value);

    void get(String key, Consumer<String> consumer);

    void expire(String key, int seconds);
}
